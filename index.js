const Metalsmith = require('metalsmith')
const markdown = require('metalsmith-markdown')
const layouts = require('metalsmith-layouts')
const permalinks = require('metalsmith-permalinks')
const postcss = require('metalsmith-with-postcss')
const rename = require('metalsmith-rename')
const copy = require('metalsmith-copy')
const sugarss = require('sugarss')

Metalsmith(__dirname)
  .metadata({
    title: 'DIV Compo',
    description: 'DIV Compo',
    author: 'AzazelN28',
    generator: {
      name: 'Metalsmith',
      url: 'http://www.metalsmith.io/'
    }
  })
  .source('./src')
  .destination('./public')
  .clean(false)
  .use(postcss({
    pattern: ['**/*.sss', '!**/_*/*', '!**/_*'], //For SugarSS,
    parser: sugarss,
    plugins: {
      'postcss-preset-env': {}
      /*
      'postcss-import': {},
      'postcss-if-media': {},
      'postcss-custom-media': {},
      'postcss-media-minmax': {},
      'postcss-layout': {},
      'postcss-aspect-ratio': {},
      'autoprefixer': {}
      */
    }
  }))
  .use(copy({
    pattern: 'images/*.(jpg|png|gif)',
    directory: 'images'
  }))
  .use(rename([[/\.sss$/, '.css']]))
  .use(markdown({
    gfm: true,
    tables: true
  }))
  .use(permalinks())
  .use(layouts({ engine: 'handlebars' }))
  .build(function(err, files) {
    if (err) {
      throw err;
    }
  })
